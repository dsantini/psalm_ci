<?php
$pw = "Hello world";

$pwDEFAULT = password_hash($pw, PASSWORD_DEFAULT);
var_dump($pwDEFAULT);
//var_dump(password_get_info($pwDEFAULT));
var_dump(password_verify($pw, $pwDEFAULT));
var_dump(password_verify("xyz", $pwDEFAULT));

$pwBCRYPT = password_hash($pw, PASSWORD_BCRYPT);
var_dump($pwBCRYPT);
//var_dump(password_get_info($pwBCRYPT));
var_dump(password_verify($pw, $pwBCRYPT));
var_dump(password_verify("xyz", $pwBCRYPT));

$pwARGON2I = password_hash($pw, PASSWORD_ARGON2I);
var_dump($pwARGON2I);
//var_dump(password_get_info($pwARGON2I));
var_dump(password_verify($pw, $pwARGON2I));
var_dump(password_verify("xyz", $pwARGON2I));

$pwARGON2ID = password_hash($pw, PASSWORD_ARGON2ID);
var_dump($pwARGON2ID);
//var_dump(password_get_info($pwARGON2ID));
var_dump(password_verify($pw, $pwARGON2ID));
var_dump(password_verify("xyz", $pwARGON2ID));