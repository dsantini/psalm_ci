REM php -f ..\git\psalm\src\psalm.php -- --taint-analysis --output-format=codeclimate index.php 1> p_codeclimate.json 2> p_err.log
REM php -f ..\git\psalm\src\psalm.php -- --taint-analysis --report=p.json index.php 1> p_out.log 2> p_err.log
php -f ..\git\psalm\src\psalm.php -- --taint-analysis --report=p.sarif index.php 1> p_out.log 2> p_err.log
REM php -f ..\git\psalm\src\psalm.php -- --taint-analysis --report=p.xml index.php 1> p_out.log 2> p_err.log