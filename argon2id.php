<?php
$timeTarget = 0.2; // 100 milliseconds 

$cost = [
    PASSWORD_ARGON2_DEFAULT_MEMORY_COST,
    PASSWORD_ARGON2_DEFAULT_TIME_COST,
    16 // PASSWORD_ARGON2_DEFAULT_THREADS
];
do {
    $cost[0] += 32768;
    $cost[1] += 1;
    $options = ["memory_cost" => $cost[0], "time_cost" => $cost[1], "threads" => $cost[2]];

    $start = microtime(true);
    password_hash("test", PASSWORD_ARGON2ID, $options);
    $end = microtime(true);

    echo  json_encode($options)." ".($end - $start).PHP_EOL;
} while (($end - $start) < $timeTarget);

echo "Appropriate Cost Found: " . json_encode($cost);
