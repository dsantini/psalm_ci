<?php
$timeTarget = 0.1; // 100 milliseconds 

$cost = 8;
do {
    $options = ["cost" => ++$cost];

    $start = microtime(true);
    password_hash("test", PASSWORD_BCRYPT, $options);
    $end = microtime(true);
    
    echo  json_encode($options)." ".($end - $start).PHP_EOL;
} while (($end - $start) < $timeTarget);

echo "Appropriate Cost Found: " . $cost;
